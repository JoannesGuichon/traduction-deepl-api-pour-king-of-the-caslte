# Traduction Deepl API pour King of the Caslte

Ce petit Git a été fait pour servir de tuto pour Mynthos pour traduire King of the Castle en direct avec l'API Deepl. Tout le monde est le bienvenu pour l'utiliser.
La façon dont tout fonctionne est que XUnity AutoTranslator va récupérer les textes pendant le jeu, les écrire dans un fichier et mettre leur traduction en même temps et update le texte du jeu en direct.

## Comment installer

C'est tout facile vous allez voir il suffit de suivre les instructions : 

1. Télécharger les fichiers du git sous forme de .zip en cliquant sur l'icone de boite avec une flèche vers le bas (puis zip)
2. Extraire les fichiers dans le répertoire de jeu de King of the Castle 
    - le chemin pour les fichiers aura la forme suivante : C:\Program Files (x86)\Steam\steamapps\common\King of the Castle
3. Lancer le .exe qui va lier le traducteur au jeu.
4. Attendre la fin de l'installation et appuyer sur entrée
5. Lancer le .exe KingOfTheCastle (Patch and Run) et fermer le jeu directement après.
6. Placer le fichier .ini de mon git à la place de celui généré de base.
6. OPTIONNEL Placer mon fichier _AutoGeneratedTranslations.txt pour avoir les trads que j'ai déjà généré en solo et que j'ai commencé à corriger
    - A placer dans le chemin C:\Program Files (x86)\Steam\steamapps\common\King of the Castle\AutoTranslator\Translation\fr\Text 
7. Lancer le jeu et normalement ça va traduire une partie. Si ça marche pas pingez moi sur Discord (on peut me contacter en MP depuis le serveur de Mynthos, mon pseudo c'est ElmirDeSang)

## Pour aller plus loin et m'aider 

Pour pouvoir améliorer la traduction de Deepl et que je puisse continuer la traduction à la main du jeu, je veux bien que vous m'envoyiez vos fichiers qui seront stockés ici : C:\Program Files (x86)\Steam\steamapps\common\King of the Castle\AutoTranslator\Translation\fr\Text
ça me permettra d'essayer de récupérer tout les textes du jeu (comme on a des textes différents en fonction de nos histoires et des parties donc plein de texte à récup).
Si des gens sont très chauds en traduction, n'hésitez pas à me contacter sur Discord (sur le Discord de Mynthos, mon pseudo c'est ElmirDeSang, envoyez moi des MP, je suis réactif).

## Authors and acknowledgment
(Fr) Je ne possède pas le code utilisé qui est forqué notamment depuis le Git de XUnity AutoTranslator. Le travail principal leur revient, j'ai juste modifié un peu leur appli de base et fait l'explication de comment brancher notre traducteur.

(English) I don't own the code used which is forked from the XUnity AutoTranslator Git. The main work is theirs, I just modified their basic application a bit and made the explanation of how to connect our translator.
## License
 
(Fr) MIT License hérité du projet de XUnity AutoTranslator.
(English) MIT License inherited from XUnity AutoTranslator project.